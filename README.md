# Getting Started

## Before Beginning
Clone the project from the repository and import it on Eclipse as Maven project. Open **match_api.sql** in sql folder and create your local PostgreSQL schema. 
Please change the connection parameters for your database in **application.yml** file in case needed. For the tests an in-memory database is used.

## Packages and classes
Here a small briefing about the project structure is presented. For detailed documentation you may take a look at the provided Javadoc on the classes.
The project contains the following packages:

 * com.accepted.interview: The package that contaings the class that starts the application 
 * com.accepted.interview.controller: Contains the classes that are responsible to receive information from and return response back to the client
 * com.accepted.interview.converter: Contains classes that perform transformations on objects
 * com.accepted.interview.exception: Contains classes that have to do with exception handling
 * com.accepted.interview.model: Contains various POJO classes
 * com.accepted.interview.model.dto: Contains various POJO classes that act as data transfer objects
 * com.accepted.interview.model.repository: Contains the entity classes that represent database tables
 * com.accepted.interview.repository: Contains classes that handle database interaction
 * com.accepted.interview.serializer: Contains classes for json serialization/deserialization rules
 * com.accepted.interview.service: Contains classes that act as the service layer of the application
 * com.accepted.interview.utils: Contains various utility classes

## API Documentation
This is a REST API for handling CRUD operations for two entities, Match and MatchOdds. In the following sections we will analyse the API endpoints and provide example calls.
The base path for the endpoint when running locally is **http://localhost:8080**.

### Matches endpoint
The base path for matches is the **/matches** endpoint. 

#### Get all mathes paginated
In order to retrieve all matches in database in a paginated format we have to make a **GET** call on **/matches** endpoint. The HTTP status code is 200 and the response looks like below:
```json
{
    "_embedded": {
        "matchDtoes": [
            {
                "description": "test",
                "matchDate": "30-10-2021",
                "matchTime": "12:00",
                "teamA": "PAO",
                "teamB": "AEK",
                "sport": "FOOTBALL",
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/matches/1"
                    }
                }
            },
            {
                "description": "test2",
                "matchDate": "29-10-2021",
                "matchTime": "12:00",
                "teamA": "OLYMPIAKOS",
                "teamB": "PAO",
                "sport": "BASKETBALL",
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/matches/2"
                    }
                }
            },
            {
                "description": "test4",
                "matchDate": "29-10-2021",
                "matchTime": "12:00",
                "teamA": "OLYMPIAKOS",
                "teamB": "PAO",
                "sport": "BASKETBALL",
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/matches/4"
                    }
                }
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/matches?page=0&size=20"
        }
    },
    "page": {
        "size": 20,
        "totalElements": 3,
        "totalPages": 1,
        "number": 0
    }
}
```
The response contains the link for each match as well links for next and previous pages if they exist.

#### Get specific match
In order to receive information about a specific match we have to make a **GET** call on **/matches/{id}** endpoint. The HTTP status code is 200 and the response looks like below:
```json
{
    "description": "test",
    "matchDate": "30-10-2021",
    "matchTime": "12:00",
    "teamA": "PAO",
    "teamB": "AEK",
    "sport": "FOOTBALL"
}
```

#### Create new match
In order to create a new match we have to make a **POST** call on **/matches** endpoint using the provided body:
```json
{
    "description": "test",
    "matchDate": "30-10-2021",
    "matchTime": "12:00",
    "teamA": "PAO",
    "teamB": "AEK",
    "sport": "FOOTBALL"
}
```
Do not forget to set the **content-type** header to **application/json**. The response of the call has no body, the HTTP status code is 201 and a location header is included with the link
of the created match. It is important to fill all the above information on the request body. If any field is missing then the response will contain an error looking like below:
```json
{
    "statusCode": 500,
    "message": "not-null property references a null or transient value : com.accepted.interview.model.repository.Match.sport; nested exception is org.hibernate.PropertyValueException: not-null property references a null or transient value : com.accepted.interview.model.repository.Match.sport"
}
```

#### Update match
In order to update an existing match we have to make a **PUT** call on **/matches/{id}** endpoint using the provided body:
```json
{
    "description": "test",
    "matchDate": "30-10-2021",
    "matchTime": "12:00",
    "teamA": "PAO",
    "teamB": "AEK",
    "sport": "FOOTBALL"
}
```
Do not forget to set the **content-type** header to **application/json**. The response of the call has no body and the HTTP status code is 204. Again, all fields must contain value.
In case you provide an invalid match id the HTTP status code is 404 and the response will look like:
```json
{
    "statusCode": 404,
    "message": "Match does not exist: 14"
}
```

#### Delete match
In order to delete an existing match we have to make a **DELETE** call on **/matches/{id}**. The response of the call has no body and the HTTP status code is 204. 
In case you provide an invalid match id the HTTP status code is 404 and the response will look like:
```json
{
    "statusCode": 404,
    "message": "Match does not exist: 14"
}
```

### Matches odds endpoint 
The base path for matches odds is the **/matches/{matchId}/odds** endpoint.

#### Get all match odds
In order to retrieve all match odds for a specific match we have to make a **GET** call on **/matches/{matchId}/odds** endpoint. The HTTP status code is 200 and the response looks like below:
```json
[
    {
        "specifier": "X",
        "odd": 1.45
    },
    {
        "specifier": "1",
        "odd": 2.35
    }
]
```

#### Get specific odd for match
In order to retrieve a specific odd for a specific match we have to make a **GET** call on **/matches/{matchId}/odds/{oddId}** endpoint. The HTTP status code is 200 and the response looks like below:
```json
{
    "specifier": "X",
    "odd": 1.45
}
```

#### Create odd for match
In order to create a new match odd we have to make a **POST** call on **/matches/{matchId}/odds** endpoint using the provided body:
```json
{
    "specifier": "X",
    "odd": 1.45
}
```

Do not forget to set the **content-type** header to **application/json**. The response of the call has no body, the HTTP status code is 201 and a location header is included with the link
of the created match odd. It is important to fill all the above information on the request body. If any field is missing then the response will contain an error looking like below:
```json
{
    "statusCode": 500,
    "message": "not-null property references a null or transient value : com.accepted.interview.model.repository.Match.sport; nested exception is org.hibernate.PropertyValueException: not-null property references a null or transient value : com.accepted.interview.model.repository.Match.sport"
}
```

In case you provide an invalid match id the HTTP status code is 404 and the response will look like:
```json
{
    "statusCode": 404,
    "message": "Match does not exist: 14"
}
```

#### Update odd for match
In order to update an existing match odd we have to make a **PUT** call on **/matches/{matchId}/odds/{oddId}** endpoint using the provided body:
```json
{
    "specifier": "X",
    "odd": 1.45
}
```

Do not forget to set the **content-type** header to **application/json**. The response of the call has no body and the HTTP status code is 204. Again, all fields must contain value.
In case you provide an invalid match id or an invalid odd id the HTTP status code is 404 and the response will look like:
```json
{
    "statusCode": 404,
    "message": "Match does not exist: 14"
}
```
In case the odd does not belong to match the HTTP status code is 500 and the response will look like:
```json
{
    "statusCode": 500,
    "message": ""Match odd with id 3 does not belong to match with id 4"
}
```

#### Delete odd from match
In order to delete an existing match odd we have to make a **DELETE** call on **/matches/{matchId}/odds/{oddId}**. The response of the call has no body and the HTTP status code is 204. 
In case you provide an invalid match id or odd id the HTTP status code is 404 and the response will look like:
```json
{
    "statusCode": 404,
    "message": "Match does not exist: 14"
}
```
In case the odd does not belong to match the HTTP status code is 500 and the response will look like:
```json
{
    "statusCode": 500,
    "message": ""Match odd with id 3 does not belong to match with id 4"
}
```