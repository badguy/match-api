-- DROP SCHEMA matchapi;

CREATE SCHEMA matchapi AUTHORIZATION postgres;

-- DROP SEQUENCE matchapi.match_odds_id_seq;

CREATE SEQUENCE matchapi.match_odds_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE matchapi.match_odds_id_seq1;

CREATE SEQUENCE matchapi.match_odds_id_seq1
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;-- matchapi."match" definition

-- Drop table

-- DROP TABLE matchapi."match";

CREATE TABLE matchapi."match" (
	id int4 NOT NULL DEFAULT nextval('matchapi.match_odds_id_seq'::regclass),
	description text NOT NULL,
	match_date date NOT NULL,
	match_time time NOT NULL,
	team_a varchar NOT NULL,
	team_b varchar NOT NULL,
	sport int4 NOT NULL,
	CONSTRAINT match_pk PRIMARY KEY (id)
);


-- matchapi.match_odds definition

-- Drop table

-- DROP TABLE matchapi.match_odds;

CREATE TABLE matchapi.match_odds (
	id serial4 NOT NULL,
	match_id int4 NOT NULL,
	specifier varchar NOT NULL,
	odd numeric NOT NULL,
	CONSTRAINT match_odds_pk PRIMARY KEY (id),
	CONSTRAINT match_odds_fk FOREIGN KEY (match_id) REFERENCES matchapi."match"(id) ON DELETE CASCADE ON UPDATE CASCADE
);
