package com.accepted.interview.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import com.accepted.interview.model.Sport;
import com.accepted.interview.model.repository.Match;

@DataJpaTest
@ActiveProfiles("test")
public class MatchRepositoryTest {

  @Autowired
  private MatchRepository matchRepository;
  
  @BeforeEach
  public void setup() {
    Match match1 = new Match();
    match1.setDescription("Match Description");
    match1.setMatchDate(LocalDate.EPOCH);
    match1.setMatchTime(LocalTime.MIDNIGHT);
    match1.setTeamA("AEK");
    match1.setTeamB("OSFP");
    match1.setSport(Sport.BASKETBALL);
    
    Match match2 = new Match();
    match2.setDescription("Match Description");
    match2.setMatchDate(LocalDate.EPOCH);
    match2.setMatchTime(LocalTime.NOON);
    match2.setTeamA("AEK");
    match2.setTeamB("PAO");
    match2.setSport(Sport.FOOTBALL);
    
    matchRepository.save(match1);
    matchRepository.save(match2);
  }
  
  @Test
  public void findAllMatchesPaged() {
    Page<Match> page = matchRepository.findAll(Pageable.ofSize(5));
    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent().stream().filter(m -> m.getSport().equals(Sport.FOOTBALL)).findFirst().get().getTeamB()).isEqualTo("PAO");
    assertThat(page.getContent().stream().filter(m -> m.getSport().equals(Sport.BASKETBALL)).findFirst().get().getTeamB()).isEqualTo("OSFP");
  }
}
