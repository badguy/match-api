package com.accepted.interview.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import com.accepted.interview.model.Sport;
import com.accepted.interview.model.dto.MatchDto;
import com.accepted.interview.model.dto.OddDto;
import com.accepted.interview.model.repository.Match;
import com.accepted.interview.model.repository.MatchOdd;
import com.accepted.interview.repository.MatchOddRepository;
import com.accepted.interview.repository.MatchRepository;

@SpringBootTest
@ActiveProfiles("test")
public class MatchServiceTest {

  @Autowired
  private MatchRepository matchRepository;

  @Autowired
  private MatchOddRepository matchOddRepository;

  @Autowired
  private MatchService matchService;

  private Match match1, match2;

  private MatchOdd odd1, odd2;

  @BeforeEach
  public void setup() {
    match1 = new Match();
    match1.setDescription("Match Description");
    match1.setMatchDate(LocalDate.EPOCH);
    match1.setMatchTime(LocalTime.MIDNIGHT);
    match1.setTeamA("AEK");
    match1.setTeamB("OSFP");
    match1.setSport(Sport.BASKETBALL);

    match2 = new Match();
    match2.setDescription("Match Description");
    match2.setMatchDate(LocalDate.EPOCH);
    match2.setMatchTime(LocalTime.NOON);
    match2.setTeamA("AEK");
    match2.setTeamB("PAO");
    match2.setSport(Sport.FOOTBALL);

    match1 = matchRepository.save(match1);
    match2 = matchRepository.save(match2);

    odd1 = new MatchOdd();
    odd1.setMatch(match1);
    odd1.setOdd(new BigDecimal("1.25"));
    odd1.setSpecifier("X");

    odd2 = new MatchOdd();
    odd2.setMatch(match2);
    odd2.setOdd(new BigDecimal("3.25"));
    odd2.setSpecifier("1");

    odd1 = matchOddRepository.save(odd1);
    odd2 = matchOddRepository.save(odd2);
  }

  @AfterEach
  public void cleanup() {
    matchRepository.deleteAll();
    matchOddRepository.deleteAll();
  }

  @Test
  public void findMatchByExistingId() {
    MatchDto dto = matchService.findMatch(match1.getId());
    assertNotNull(dto);
    assertThat(dto.getDescription()).isEqualTo("Match Description");
    assertThat(dto.getMatchDate()).isEqualTo(LocalDate.EPOCH);
    assertThat(dto.getMatchTime()).isEqualTo(LocalTime.MIDNIGHT);
    assertThat(dto.getTeamA()).isEqualTo("AEK");
    assertThat(dto.getTeamB()).isEqualTo("OSFP");
    assertThat(dto.getSport()).isEqualTo(Sport.BASKETBALL);
  }

  @Test
  public void findMatchByNonExistingIdThrowsException() {
    Exception exception = assertThrows(NoSuchElementException.class, () -> {
      matchService.findMatch(match2.getId() + 1);
    });

    long id = match2.getId() + 1;
    String expectedMessage = "Match does not exist: " + id;
    String actualMessage = exception.getMessage();
    assertEquals(actualMessage, expectedMessage);
  }

  @Test
  public void findAllMatches() {
    Page<Match> page = matchService.findAllMatches(Pageable.ofSize(5));
    assertThat(page.getContent().size()).isEqualTo(2);
  }

  @Test
  public void createMatch() {
    match2.setId(null);
    Long match3Id = matchService.createMatch(new MatchDto(match2));
    MatchDto match3 = matchService.findMatch(match3Id);
    assertNotNull(match3);
  }

  @Test
  public void updateMatch() {
    match2.setSport(Sport.BASKETBALL);
    matchService.updateMatch(match2.getId(), new MatchDto(match2));
    MatchDto dto = matchService.findMatch(match2.getId());
    assertThat(dto.getSport()).isEqualTo(Sport.BASKETBALL);
  }

  @Test
  public void updateMatchFailsForNonExistingId() {
    Exception exception = assertThrows(NoSuchElementException.class, () -> {
      matchService.updateMatch(match2.getId() + 1, new MatchDto(match2));
    });

    long id = match2.getId() + 1;
    String expectedMessage = "Match does not exist: " + id;
    String actualMessage = exception.getMessage();
    assertEquals(actualMessage, expectedMessage);
  }

  @Test
  public void deleteMatch() {
    matchService.deleteMatch(match2.getId());
    assertThrows(NoSuchElementException.class, () -> {
      matchService.findMatch(match2.getId());
    });
  }

  @Test
  public void findOddsForMatch() {
    List<OddDto> dtos = matchService.findOddsForMatch(match1.getId());
    assertEquals(dtos.size(), 1);
    assertThat(dtos.get(0).getOdd().toString()).isEqualTo("1.25");
    assertThat(dtos.get(0).getSpecifier()).isEqualTo("X");
  }

  @Test
  public void findOddsForNonExistingMatchFails() {
    assertThrows(NoSuchElementException.class, () -> {
      matchService.findOddsForMatch(match2.getId() + 1);
    });
  }

  @Test
  public void findOddForMatch() {
    OddDto dto = matchService.findOddForMatch(match1.getId(), odd1.getId());
    assertNotNull(dto);
    assertThat(dto.getOdd().toString()).isEqualTo("1.25");
    assertThat(dto.getSpecifier()).isEqualTo("X");
  }

  @Test
  public void findOddForMatchFailsWhenNotBelongingToMatch() {
    Exception exception = assertThrows(NoSuchElementException.class, () -> {
      matchService.findOddForMatch(match1.getId(), odd2.getId());
    });

    String expectedMessage = "Match odd with id " + odd2.getId() + " does not exist for match: "
        + match1.getId();
    String actualMessage = exception.getMessage();
    assertEquals(actualMessage, expectedMessage);
  }

  @Test
  public void createOddForMatch() {
    odd1.setId(null);
    Long oddId = matchService.createOddForMatch(match1.getId(), new OddDto(odd2));
    OddDto dto = matchService.findOddForMatch(match1.getId(), oddId);
    assertNotNull(dto);
  }

  @Test
  public void createOddForNotExistingMatchFails() {
    assertThrows(NoSuchElementException.class, () -> {
      matchService.createOddForMatch(match2.getId() + 1, new OddDto(odd2));
    });
  }

  @Test
  public void updateMatchOdd() {
    odd2.setSpecifier("X");
    matchService.updateMatchOdd(match2.getId(), odd2.getId(), new OddDto(odd2));
    OddDto dto = matchService.findOddForMatch(match2.getId(), odd2.getId());
    assertThat(dto.getSpecifier()).isEqualTo("X");
  }

  @Test
  public void updateMatchOddFails() {
    assertThrows(NoSuchElementException.class, () -> {
      matchService.updateMatchOdd(match2.getId() + 1, odd2.getId(), new OddDto(odd2));
    });

    assertThrows(NoSuchElementException.class, () -> {
      matchService.updateMatchOdd(match2.getId(), odd2.getId() + 1, new OddDto(odd2));
    });

    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
      matchService.updateMatchOdd(match1.getId(), odd2.getId(), new OddDto(odd2));
    });

    String expectedMessage = "Match odd with id " + odd2.getId()
        + " does not belong to match with id " + match1.getId();
    String actualMessage = exception.getMessage();
    assertEquals(actualMessage, expectedMessage);
  }
  
  @Test
  public void deleteMatchOdd() {
    matchService.deleteMatchOdd(match2.getId(), odd2.getId());
    assertThrows(NoSuchElementException.class, () -> {
      matchService.findOddForMatch(match2.getId(), odd2.getId());
    });
  }
  
  @Test
  public void deleteMatchOddFails() {
    assertThrows(NoSuchElementException.class, () -> {
      matchService.deleteMatchOdd(match2.getId() + 1, odd2.getId());
    });

    assertThrows(NoSuchElementException.class, () -> {
      matchService.deleteMatchOdd(match2.getId(), odd2.getId() + 1);
    });

    Exception exception = assertThrows(IllegalArgumentException.class, () -> {
      matchService.deleteMatchOdd(match1.getId(), odd2.getId());
    });

    String expectedMessage = "Match odd with id " + odd2.getId()
        + " does not belong to match with id " + match1.getId();
    String actualMessage = exception.getMessage();
    assertEquals(actualMessage, expectedMessage);
  }
}
