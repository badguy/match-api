package com.accepted.interview.model.dto;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.accepted.interview.model.Sport;
import com.accepted.interview.model.repository.Match;


public class MatchDtoTest {
  
  private static MatchDto matchDto;

  @BeforeAll
  public static void setup() {
    Match match = new Match();
    match.setDescription("Match Description");
    match.setMatchDate(LocalDate.EPOCH);
    match.setMatchTime(LocalTime.MIDNIGHT);
    match.setTeamA("AEK");
    match.setTeamB("OSFP");
    match.setSport(Sport.BASKETBALL);
    matchDto = new MatchDto(match);
  }
  
  @Test
  public void checkMatchDto() {
    assertThat(matchDto.getDescription()).isEqualTo("Match Description");
    assertThat(matchDto.getMatchDate()).isEqualTo(LocalDate.EPOCH);
    assertThat(matchDto.getMatchTime()).isEqualTo(LocalTime.MIDNIGHT);
    assertThat(matchDto.getTeamA()).isEqualTo("AEK");
    assertThat(matchDto.getTeamB()).isEqualTo("OSFP");
    assertThat(matchDto.getSport()).isEqualTo(Sport.BASKETBALL);
  }
}
