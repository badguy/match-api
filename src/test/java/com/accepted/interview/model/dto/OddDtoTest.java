package com.accepted.interview.model.dto;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.accepted.interview.model.repository.MatchOdd;

public class OddDtoTest {

  private static OddDto oddDto;

  @BeforeAll
  public static void setup() {
    MatchOdd odd = new MatchOdd();
    odd.setOdd(new BigDecimal("1.86"));
    odd.setSpecifier("X");
    oddDto = new OddDto(odd);
  }
  
  @Test
  public void checkOddDto() {
    assertThat(oddDto.getOdd().toString()).isEqualTo("1.86");
    assertThat(oddDto.getSpecifier()).isEqualTo("X");
  }
}
