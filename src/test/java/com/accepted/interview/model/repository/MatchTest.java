package com.accepted.interview.model.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.accepted.interview.model.Sport;
import com.accepted.interview.model.dto.MatchDto;

public class MatchTest {

  private static Match match;
  private static MatchDto dto;
  
  @BeforeAll
  public static void setup() {
    dto = new MatchDto();
    dto.setDescription("Match Description");
    dto.setMatchDate(LocalDate.EPOCH);
    dto.setMatchTime(LocalTime.NOON);
    dto.setTeamA("AEK");
    dto.setTeamB("OSFP");
    dto.setSport(Sport.FOOTBALL);
    match = new Match(dto);  
  }
  
  @Test
  public void checkMatch() {
    assertThat(match.getDescription()).isEqualTo("Match Description");
    assertThat(match.getMatchDate()).isEqualTo(LocalDate.EPOCH);
    assertThat(match.getMatchTime()).isEqualTo(LocalTime.NOON);
    assertThat(match.getTeamA()).isEqualTo("AEK");
    assertThat(match.getTeamB()).isEqualTo("OSFP");
    assertThat(match.getSport()).isEqualTo(Sport.FOOTBALL);
  }
  
  @Test
  public void checkMatchisUpdatedSuccessfully() {
    match = new Match();
    match.updateFrom(dto);
    assertThat(match.getDescription()).isEqualTo("Match Description");
    assertThat(match.getMatchDate()).isEqualTo(LocalDate.EPOCH);
    assertThat(match.getMatchTime()).isEqualTo(LocalTime.NOON);
    assertThat(match.getTeamA()).isEqualTo("AEK");
    assertThat(match.getTeamB()).isEqualTo("OSFP");
    assertThat(match.getSport()).isEqualTo(Sport.FOOTBALL);
  }
}
