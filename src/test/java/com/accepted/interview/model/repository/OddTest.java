package com.accepted.interview.model.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.accepted.interview.model.dto.OddDto;

public class OddTest {

  private static MatchOdd matchOdd;
  private static OddDto dto;
  
  @BeforeAll
  public static void setup() {
    dto = new OddDto();
    dto.setOdd(new BigDecimal("2.36"));
    dto.setSpecifier("X");
    matchOdd = new MatchOdd(null, dto);
  }
  
  @Test
  public void checkMatchOdd() {
    assertThat(matchOdd.getOdd().toString()).isEqualTo("2.36");
    assertThat(matchOdd.getSpecifier()).isEqualTo("X");
  }
  
  @Test
  public void checkMatchOddisUpdatedSuccessfully() {
    matchOdd = new MatchOdd();
    matchOdd.updateFrom(null, dto);
    assertThat(matchOdd.getOdd().toString()).isEqualTo("2.36");
    assertThat(matchOdd.getSpecifier()).isEqualTo("X");
  }
}
