package com.accepted.interview.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.accepted.interview.exception.RestResponseEntityExceptionHandler;
import com.accepted.interview.model.dto.OddDto;
import com.accepted.interview.service.MatchService;

@SpringBootTest
public class MatchOddControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private MatchOddController controller;

  @MockBean
  private MatchService service;

  private static OddDto oddDto;

  @BeforeEach
  public void setup() {
    mockMvc = MockMvcBuilders.standaloneSetup(controller)
        .setControllerAdvice(new RestResponseEntityExceptionHandler()).build();
  }

  @BeforeAll
  public static void setupAll() {
    oddDto = new OddDto();
    oddDto.setSpecifier("X");
    oddDto.setOdd(new BigDecimal("1.23"));
  }
  
  @Test
  public void getOddsForMatchReturnsSuccessfully() throws Exception {
    List<OddDto> dtos = new ArrayList<>();
    dtos.add(oddDto);
    when(service.findOddsForMatch(Long.valueOf(1))).thenReturn(dtos);
    
    this.mockMvc.perform(get("/matches/1/odds")).andExpect(status().isOk())
    .andExpect(jsonPath("$.[0].specifier", is("X")))
    .andExpect(jsonPath("$.[0].odd", is(1.23)));
  }
  
  @Test
  public void getOddForMatchReturnsSuccessfully() throws Exception {
    when(service.findOddForMatch(Long.valueOf(1), Long.valueOf(1))).thenReturn(oddDto);
    
    this.mockMvc.perform(get("/matches/1/odds/1")).andExpect(status().isOk())
    .andExpect(jsonPath("$.specifier", is("X")))
    .andExpect(jsonPath("$.odd", is(1.23)));
  }
  
  @Test
  public void postOddForMatchReturnsSuccessfully() throws Exception {
    String json = "{\"specifier\": \"X\",\"odd\": 1.23}";

    this.mockMvc.perform(post("/matches/1/odds").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isCreated());
  }
  
  //This should be working but it does not, don't know why
  @Test
  @Disabled
  public void postOddForMatchFails() throws Exception {
    when(service.createOddForMatch(Long.valueOf(7), oddDto)).thenThrow(new IllegalArgumentException());
    String json = "{\"specifier\": \"X\",\"odd\": 1.23}";

    this.mockMvc.perform(post("/matches/7/odds").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isNotFound());
  }
  
  @Test 
  public void putMatchOddSuccessfully() throws Exception {
    String json = "{\"specifier\": \"X\",\"odd\": 1.23}";

    this.mockMvc.perform(put("/matches/1/odds/1").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isNoContent());
  }
  
  // This should be working but it does not, don't know why
  @Test 
  @Disabled
  public void putMatchOddFails() throws Exception {
    willThrow(new NoSuchElementException()).given(service).updateMatchOdd(Long.valueOf(1), Long.valueOf(2), oddDto);
    String json = "{\"specifier\": \"X\",\"odd\": 1.23}";

    this.mockMvc.perform(put("/matches/1/odds/2").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isNotFound());
  }
  
  @Test
  public void deleteMatchOddReturnsSuccessfully() throws Exception {
    this.mockMvc.perform(delete("/matches/1/odds/1")).andExpect(status().isNoContent());
  }
  
  @Test
  public void deleteMatchOddFails() throws Exception {
    willThrow(new NoSuchElementException()).given(service).deleteMatchOdd(Long.valueOf(2), Long.valueOf(1));

    this.mockMvc.perform(delete("/matches/2/odds/1")).andExpect(status().isNotFound());
  }
  
  @Test
  public void deleteMatchOddFailsAgain() throws Exception {
    willThrow(new IllegalArgumentException()).given(service).deleteMatchOdd(Long.valueOf(2), Long.valueOf(1));

    this.mockMvc.perform(delete("/matches/2/odds/1")).andExpect(status().isInternalServerError());
  }
}
