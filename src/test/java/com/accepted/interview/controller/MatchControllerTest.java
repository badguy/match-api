package com.accepted.interview.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.accepted.interview.exception.RestResponseEntityExceptionHandler;
import com.accepted.interview.model.Sport;
import com.accepted.interview.model.dto.MatchDto;
import com.accepted.interview.service.MatchService;

@SpringBootTest
public class MatchControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private MatchController controller;

  @MockBean
  private MatchService service;

  private static MatchDto matchDto;

  @BeforeEach
  public void setup() {
    mockMvc = MockMvcBuilders.standaloneSetup(controller)
        .setControllerAdvice(new RestResponseEntityExceptionHandler()).build();
  }

  @BeforeAll
  public static void setupAll() {
    matchDto = new MatchDto();
    matchDto.setMatchDate(LocalDate.EPOCH);
    matchDto.setDescription("Description");
    matchDto.setMatchTime(LocalTime.NOON);
    matchDto.setTeamA("AEK");
    matchDto.setTeamB("OSFP");
    matchDto.setSport(Sport.BASKETBALL);
  }

  @Test
  public void getMatchForIdReturnsSuccessfully() throws Exception {
    when(service.findMatch(Long.valueOf(1))).thenReturn(matchDto);

    this.mockMvc.perform(get("/matches/1")).andExpect(status().isOk())
        .andExpect(jsonPath("$.description", is("Description")))
        .andExpect(jsonPath("$.matchDate", is("01-01-1970")))
        .andExpect(jsonPath("$.matchTime", is("12:00"))).andExpect(jsonPath("$.teamA", is("AEK")))
        .andExpect(jsonPath("$.teamB", is("OSFP")))
        .andExpect(jsonPath("$.sport", is("BASKETBALL")));
  }

  @Test
  public void postMatchReturnsSuccessfully() throws Exception {
    String json = "{\"description\":\"Description\",\"matchDate\":\"01-01-1970\",\"matchTime\":\"12:00\",\"teamA\":\"AEK\",\"teamB\":\"OSFP\",\"sport\":\"BASKETBALL\"}";

    this.mockMvc.perform(post("/matches").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isCreated());
  }

  @Test
  public void putMatchReturnsSuccessfully() throws Exception {
    String json = "{\"description\":\"Description\",\"matchDate\":\"01-01-1970\",\"matchTime\":\"12:00\",\"teamA\":\"AEK\",\"teamB\":\"OSFP\",\"sport\":\"BASKETBALL\"}";

    this.mockMvc.perform(put("/matches/1").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isNoContent());
  }

  @Test
  public void putMatchFails() throws Exception {
    willThrow(new NoSuchElementException()).given(service).updateMatch(Long.valueOf(2), matchDto);
    String json = "{\"description\":\"Description\",\"matchDate\":\"01-01-1970\",\"matchTime\":\"12:00\",\"teamA\":\"AEK\",\"teamB\":\"OSFP\",\"sport\":\"BASKETBALL\"}";

    this.mockMvc.perform(put("/matches/2").contentType(MediaType.APPLICATION_JSON).content(json))
        .andExpect(status().isNotFound());
  }

  @Test
  public void deleteMatchReturnsSuccessfully() throws Exception {
    this.mockMvc.perform(delete("/matches/1")).andExpect(status().isNoContent());
  }
  
  @Test
  public void deleteMatchFails() throws Exception {
    willThrow(new NoSuchElementException()).given(service).deleteMatch(Long.valueOf(2));

    this.mockMvc.perform(delete("/matches/2")).andExpect(status().isNotFound());
  }
}
