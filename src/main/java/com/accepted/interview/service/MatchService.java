package com.accepted.interview.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accepted.interview.model.dto.MatchDto;
import com.accepted.interview.model.dto.OddDto;
import com.accepted.interview.model.repository.Match;
import com.accepted.interview.model.repository.MatchOdd;
import com.accepted.interview.repository.MatchOddRepository;
import com.accepted.interview.repository.MatchRepository;

/**
 * Acts as the service layer between the controllers and the repositories.
 * 
 * @author Nikos Nodarakis
 *
 */
@Service
@Transactional(readOnly = true)
public class MatchService {

  private MatchRepository matchRepository;
  private MatchOddRepository matchOddRepository;

  /**
   * Constructor to initialize the repositories to use.
   * 
   * @param matchRepository
   *          the repository for matches
   * @param matchOddsRepository
   *          the repository for match odds
   */
  @Autowired
  public MatchService(MatchRepository matchRepository, MatchOddRepository matchOddsRepository) {
    this.matchRepository = matchRepository;
    this.matchOddRepository = matchOddsRepository;
  }

  /**
   * Find a match using the provided id.
   * 
   * @param id
   *          the match id to look for
   * @return a {@link MatchDto} object
   */
  public MatchDto findMatch(Long id) {
    return new MatchDto(verifyMatchExists(id));
  }

  /**
   * Find all matches using pagination.
   * 
   * @param pageable
   *          the pagination information to use during the retrieval
   * @return a {@link Page} of {@link Match} objects
   */
  public Page<Match> findAllMatches(Pageable pageable) {
    return matchRepository.findAll(pageable);
  }

  /**
   * Creates a new match on database.
   * 
   * @param matchDto
   *          the match information to store
   * @return the id of the newly created match entity
   */
  @Transactional
  public Long createMatch(MatchDto matchDto) {
    return matchRepository.save(new Match(matchDto)).getId();
  }

  /**
   * Updates a match on database.
   * 
   * @param matchDto
   *          the match information to use for update
   * @throws NoSuchElementException
   *           if match does not exist on database
   */
  @Transactional
  public void updateMatch(Long matchId, MatchDto matchDto) {
    Match match = verifyMatchExists(matchId);
    match.updateFrom(matchDto);
    matchRepository.save(match);
  }

  /**
   * Deletes a match from database.
   * 
   * @param matchId
   *          the id of match to delete
   */
  @Transactional
  public void deleteMatch(Long matchId) {
    matchRepository.deleteById(matchId);
  }

  /**
   * Finds all odds for the provided match id.
   * 
   * @param matchId
   *          the match id to look odds for
   * @return a list of {@link OddDto} objects
   * @throws NoSuchElementException
   *           if match does not exist on database
   */
  public List<OddDto> findOddsForMatch(Long matchId) {
    return verifyMatchExists(matchId).getMatchOdds().stream().map(OddDto::new)
        .collect(Collectors.toList());
  }

  /**
   * Finds a specific odd that belongs to the provided match id.
   * 
   * @param matchId
   *          the match id that the odd belongs to
   * @param id
   *          the id of the odd to look for
   * @return an {@link OddDto} object
   * @throws NoSuchElementException
   *           if match or odd do not exist on database
   */
  public OddDto findOddForMatch(Long matchId, Long id) {
    MatchOdd matchOdd = verifyMatchExists(matchId).getMatchOdds().stream()
        .filter(odd -> odd.getId().equals(id)).findFirst()
        .orElseThrow(() -> new NoSuchElementException(
            "Match odd with id " + id + " does not exist for match: " + matchId));
    return new OddDto(matchOdd);
  }

  /**
   * Create an odd for the provided match id.
   * 
   * @param matchId
   *          the id of match the odd belongs to
   * @param oddDto
   *          the odd information to store
   * @return the id of the created odd
   * @throws NoSuchElementException
   *           if match does not exist on database
   */
  @Transactional
  public Long createOddForMatch(Long matchId, OddDto oddDto) {
    Match match = verifyMatchExists(matchId);
    MatchOdd matchOdd = new MatchOdd(match, oddDto);
    return matchOddRepository.save(matchOdd).getId();
  }

  /**
   * Update an odd for the provided match id.
   * 
   * @param matchId
   *          the id of match the odd belongs to
   * @param oddId
   *          the id of odd to update
   * @param oddDto
   *          the odd information to use for update
   * @throws NoSuchElementException
   *           if match or odd does not exist on database
   * @throws IllegalArgumentException
   *           if odd does not belong to match
   */
  @Transactional
  public void updateMatchOdd(Long matchId, Long oddId, OddDto oddDto) {
    Match match = verifyMatchExists(matchId);
    MatchOdd matchOdd = verifyMatchOddExists(oddId);
    if (oddBelongsToMatch(match, matchOdd)) {
      matchOdd.updateFrom(match, oddDto);
      matchOddRepository.save(matchOdd);
    } else {
      throw new IllegalArgumentException("Match odd with id " + matchOdd.getId()
          + " does not belong to match with id " + match.getId());
    }
  }

  /**
   * Deletes an odd from the provided match id.
   * 
   * @param matchId
   *          the id of match the odd belongs to
   * @param oddId
   *          the id of odd to delete
   * @throws NoSuchElementException
   *           if match or odd does not exist on database
   * @throws IllegalArgumentException
   *           if odd does not belong to match
   */
  @Transactional
  public void deleteMatchOdd(Long matchId, Long oddId) {
    Match match = verifyMatchExists(matchId);
    MatchOdd matchOdd = verifyMatchOddExists(oddId);
    if (oddBelongsToMatch(match, matchOdd)) {
      matchOddRepository.deleteById(oddId);
    } else {
      throw new IllegalArgumentException("Match odd with id " + matchOdd.getId()
          + " does not belong to match with id " + match.getId());
    }
  }

  private Match verifyMatchExists(Long matchId) {
    return matchRepository.findById(matchId)
        .orElseThrow(() -> new NoSuchElementException("Match does not exist: " + matchId));
  }

  private MatchOdd verifyMatchOddExists(Long oddId) {
    return matchOddRepository.findById(oddId)
        .orElseThrow(() -> new NoSuchElementException("Match odd does not exist: " + oddId));
  }

  private boolean oddBelongsToMatch(Match match, MatchOdd matchOdd) {
    return matchOdd.getMatch().getId().equals(match.getId());
  }
}
