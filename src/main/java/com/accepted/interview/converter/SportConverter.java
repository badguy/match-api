package com.accepted.interview.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.accepted.interview.model.Sport;

/**
 * Converter class that transforms a {@link Sport} object to its integer code in
 * order to save it to the database. The opposite task is performed when the
 * column is retrieved from the database and it is converted back to a
 * {@link Sport} instance.
 * 
 * @author Nikos Nodarakis
 *
 */
@Converter(autoApply = true)
public class SportConverter implements AttributeConverter<Sport, Integer> {

  @Override
  public Integer convertToDatabaseColumn(Sport sport) {
    if (sport == null) {
      return null;
    }
    return sport.getCode();
  }

  @Override
  public Sport convertToEntityAttribute(Integer code) {
    if (code == null) {
      return null;
    }

    return Stream.of(Sport.values()).filter(c -> c.getCode().equals(code)).findFirst()
        .orElseThrow(IllegalArgumentException::new);
  }
}