package com.accepted.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Application Class of Match API application. This application is
 * responsible for:
 * <ul>
 * <li>Handling the CRUD operations for Match REST entities</li>
 * <li>Handling the CRUD operations for MatchOdd REST entities</li>
 * <li>Validating the API calls and returning the appropiate answers to the
 * user</li>
 * </ul>
 * 
 * @author Nikos Nodarakis
 *
 */
@SpringBootApplication
public class MatchApiApplication {

  /**
   * Main method of the Application Class of Match API application for Accepted
   * technical assessment.
   * 
   * @param args
   *          The runtime arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(MatchApiApplication.class, args);
  }

}
