package com.accepted.interview.utils;

import java.net.URI;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Utility class for servlet operations.
 * 
 * @author Nikos Nodarakis
 *
 */
public class ServletUtils {

  /**
   * Builds a {@link URI} from the current request and it appends an id to the
   * path after it.
   * 
   * @param id the id to append to path
   * @return a {@link URI} instance
   */
  public static URI createLocationFor(Long id) {
    return ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id)
        .toUri();
  }
}
