package com.accepted.interview.exception;

import java.util.NoSuchElementException;

import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.accepted.interview.model.ErrorResponse;

/**
 * Class for handling exceptions that are raised from the controller classes and
 * returns the appropriate error response back to the client.
 * 
 * @author Nikos Nodarakis
 *
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  /**
   * Handle exceptions that declare an illegal state, such as
   * {@link PropertyValueException} which means the validation of a posted
   * object failed.
   * 
   * @param ex
   *          the raised exception
   * @param request
   *          the request that created the exception
   * @return the error response back to the client
   */
  @ExceptionHandler(value = { IllegalArgumentException.class, PropertyValueException.class })
  protected ResponseEntity<ErrorResponse> handleIllegalArgument(RuntimeException ex,
      WebRequest request) {
    return new ResponseEntity<ErrorResponse>(
        new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage()),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Handle exceptions that declare an element absence, such as
   * {@link NoSuchElementException}.
   * 
   * @param ex
   *          the raised exception
   * @param request
   *          the request that created the exception
   * @return the error response back to the client
   */
  @ExceptionHandler(value = { NoSuchElementException.class })
  protected ResponseEntity<ErrorResponse> handleElementAbsence(RuntimeException ex,
      WebRequest request) {
    return new ResponseEntity<ErrorResponse>(
        new ErrorResponse(HttpStatus.NOT_FOUND.value(), ex.getMessage()), HttpStatus.NOT_FOUND);
  }
}
