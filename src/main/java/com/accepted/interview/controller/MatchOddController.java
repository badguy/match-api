package com.accepted.interview.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accepted.interview.model.dto.OddDto;
import com.accepted.interview.service.MatchService;
import com.accepted.interview.utils.ServletUtils;

/**
 * REST Controller to handle CRUD operations regarding match odd entities. Each
 * method calls the appropriate method on the service layer and returns the
 * response back to the client.
 * 
 * @author Nikos Nodarakis
 *
 */
@RestController
@RequestMapping("/matches/{matchId}/odds")
public class MatchOddController {

  private MatchService matchService;

  /**
   * Constructor to initialize the service layer object .
   * 
   * @param matchService
   *          the service layer object to handle interaction with database
   */
  @Autowired
  public MatchOddController(MatchService matchService) {
    this.matchService = matchService;
  }

  /**
   * Retrieves all odds for the provided match id.
   * 
   * @param matchId
   *          the id of match to look for odds
   * @return a list of {@link OddDto} objects
   */
  @GetMapping
  public ResponseEntity<List<OddDto>> getOddsForMatch(
      @PathVariable(name = "matchId") Long matchId) {
    return new ResponseEntity<>(matchService.findOddsForMatch(matchId), HttpStatus.OK);
  }

  /**
   * Retrieves a specific odd for the provided match id.
   * 
   * @param matchId
   *          the id of match to look for odds
   * @param id
   *          the id of the odd to look for
   * @return a {@link OddDto} object
   */
  @GetMapping("/{id}")
  public ResponseEntity<OddDto> getOddForMatch(@PathVariable(name = "matchId") Long matchId,
      @PathVariable(name = "id") Long id) {
    return new ResponseEntity<>(matchService.findOddForMatch(matchId, id), HttpStatus.OK);
  }

  /**
   * Creates a new match odd entity that belongs to a match. Before proceeding
   * with the entity a validation check is performed, and if it fails the
   * appropriate message is returned to the client.
   * 
   * @param matchId
   *          the id of match that the odd will belong to
   * @param oddDto
   *          the entity information to store
   * @return an empty body, along with the location header of the newly created
   *         entity
   */
  @PostMapping
  public ResponseEntity<Void> createMatchOdd(@PathVariable(name = "matchId") Long matchId,
      @RequestBody @Validated OddDto oddDto) {
    Long oddId = matchService.createOddForMatch(matchId, oddDto);
    URI location = ServletUtils.createLocationFor(oddId);
    return ResponseEntity.created(location).build();
  }

  /**
   * Updates the match odd entity with the provided id. Before proceeding with
   * the entity a validation check is performed, and if it fails the appropriate
   * message is returned to the client.
   * 
   * @param matchId
   *          the id of match that the odd belongs to
   * @param id
   *          the id of the odd
   * @param matchDto
   *          the entity information to use to perform the update
   * @return an empty body with a success HTTP code
   */
  @PutMapping("/{id}")
  public ResponseEntity<Void> updateMatchOdd(@PathVariable(name = "matchId") Long matchId,
      @PathVariable(name = "id") Long id, @RequestBody @Validated OddDto oddDto) {
    matchService.updateMatchOdd(matchId, id, oddDto);
    return ResponseEntity.noContent().build();
  }

  /**
   * Deletes the match odd with the provided id.
   * 
   * @param matchId
   *          the id of match where the odd to delete belongs to
   * @param id
   *          the id of odd to delete
   * @return an empty body with a success HTTP code
   */
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteMatchOdd(@PathVariable(name = "matchId") Long matchId,
      @PathVariable(name = "id") Long id) {
    matchService.deleteMatchOdd(matchId, id);
    return ResponseEntity.noContent().build();
  }
}
