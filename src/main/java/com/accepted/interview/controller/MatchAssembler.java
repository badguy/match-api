package com.accepted.interview.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import com.accepted.interview.model.dto.MatchDto;
import com.accepted.interview.model.repository.Match;

/**
 * Match Assembler, convert {@link Match} to a Hateoas Supported
 * {@link MatchDto} class.
 *
 * @author Nikos Nodarakis
 * 
 */
@Component
public class MatchAssembler extends RepresentationModelAssemblerSupport<Match, MatchDto> {

  /**
   * Class constructor to initialize object properly.
   * 
   * @param entityLinks
   *          the links for repository classes
   */
  public MatchAssembler(RepositoryEntityLinks entityLinks) {
    super(MatchController.class, MatchDto.class);
  }

  @Override
  public MatchDto toModel(Match match) {
    MatchDto dto = new MatchDto(match);

    WebMvcLinkBuilder ratingLink = linkTo(
        methodOn(MatchController.class).getMatchForId(match.getId()));
    dto.add(ratingLink.withSelfRel());
    return dto;
  }
}
