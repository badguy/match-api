package com.accepted.interview.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accepted.interview.model.dto.MatchDto;
import com.accepted.interview.model.repository.Match;
import com.accepted.interview.service.MatchService;
import com.accepted.interview.utils.ServletUtils;

/**
 * REST Controller to handle CRUD operations regarding match entities. Each
 * method calls the appropriate method on the service layer and returns the
 * response back to the client.
 * 
 * @author Nikos Nodarakis
 *
 */
@RestController
@RequestMapping("/matches")
public class MatchController {

  private MatchAssembler matchAssembler;
  private MatchService matchService;

  /**
   * Constructor to initialize the service layer object and the assembler
   * object.
   * 
   * @param matchService
   *          the service layer object to handle interaction with database
   * @param assembler
   *          the assembler object, see {@link MatchAssembler}
   */
  @Autowired
  public MatchController(MatchService matchService, MatchAssembler assembler) {
    this.matchAssembler = assembler;
    this.matchService = matchService;
  }

  /**
   * Retrieves all information for all existing matches using pagination.
   * 
   * @param pageable
   *          the page parameters
   * @param pagedAssembler
   *          the assembler to create the page of the results
   * @return a page of {@link MatchDto} objects
   */
  @GetMapping
  public ResponseEntity<PagedModel<MatchDto>> getAllMatches(Pageable pageable,
      PagedResourcesAssembler<Match> pagedAssembler) {
    return new ResponseEntity<>(
        pagedAssembler.toModel(matchService.findAllMatches(pageable), matchAssembler),
        HttpStatus.OK);
  }

  /**
   * Retrieves match information for the provided match id.
   * 
   * @param id
   *          the id of the match to look for
   * @return a {@link MatchDto} object
   */
  @GetMapping("/{id}")
  public ResponseEntity<MatchDto> getMatchForId(@PathVariable(name = "id") Long id) {
    return new ResponseEntity<>(matchService.findMatch(id), HttpStatus.OK);
  }

  /**
   * Creates a new match entity. Before proceeding with the entity a validation
   * check is performed, and if it fails the appropriate message is returned to
   * the client.
   * 
   * @param matchDto
   *          the entity information to store
   * @return an empty body, along with the location header of the newly created
   *         entity
   */
  @PostMapping
  public ResponseEntity<Void> createMatch(@RequestBody @Validated MatchDto matchDto) {
    Long matchId = matchService.createMatch(matchDto);
    URI location = ServletUtils.createLocationFor(matchId);
    return ResponseEntity.created(location).build();
  }

  /**
   * Updates the match entity with the provided id. Before proceeding with the
   * entity a validation check is performed, and if it fails the appropriate
   * message is returned to the client.
   * 
   * @param matchDto
   *          the entity information to use to perform the update
   * @return an empty body with a success HTTP code
   */
  @PutMapping("/{id}")
  public ResponseEntity<Void> updateMatch(@PathVariable(name = "id") Long id,
      @RequestBody @Validated MatchDto matchDto) {
    matchService.updateMatch(id, matchDto);
    return ResponseEntity.noContent().build();
  }

  /**
   * Deletes the match with the provided id.
   * 
   * @param id
   *          the id of entity to delete
   * @return an empty body with a success HTTP code
   */
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteMatch(@PathVariable(name = "id") Long id) {
    matchService.deleteMatch(id);
    return ResponseEntity.noContent().build();
  }
}
