package com.accepted.interview.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.accepted.interview.model.repository.Match;

/**
 * Repository for abstracting database operations for the match table. It
 * extends the {@link JpaRepository} to support pagination and sorting
 * operations.
 * 
 * @author Nikos Nodarakis
 *
 */
@RepositoryRestResource(exported = false)
public interface MatchRepository extends JpaRepository<Match, Long> {

  /**
   * Find all matches given the provided page information.
   * 
   * @return a {@link Page} of {@link Match} objects
   */
  Page<Match> findAll(Pageable pageable);
}
