package com.accepted.interview.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.accepted.interview.model.repository.MatchOdd;

/**
 * Repository for abstracting database operations for the match odds table. 
 * 
 * @author Nikos Nodarakis
 *
 */
@RepositoryRestResource(exported = false)
public interface MatchOddRepository extends CrudRepository<MatchOdd, Long> {
  
}
