package com.accepted.interview.serializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;

/**
 * Deserializer class for {@link BigDecimal} objects when read from json. It is
 * used in order to scale and round the decimal digits to 2.
 * 
 * @author Nikos Nodarakis
 *
 */
public class BigDecimalDeserializer extends NumberDeserializers.BigDecimalDeserializer {

  private static final long serialVersionUID = 1L;

  @Override
  public BigDecimal deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    BigDecimal value = super.deserialize(p, ctxt);
    return value.setScale(2, RoundingMode.HALF_UP);
  }
}
