package com.accepted.interview.model;

/**
 * Pojo class that is used to convey the information about an error that
 * occurred.
 * 
 * @author Nikos Nodarakis
 *
 */
public class ErrorResponse {

  private int statusCode;
  private String message;

  /**
   * Constructor using fields.
   * 
   * @param statusCode
   *          the HTTP status code of the request that failed
   * @param message
   *          a description explaining the failure
   */
  public ErrorResponse(int statusCode, String message) {
    this.statusCode = statusCode;
    this.message = message;
  }

  /**
   * @return the statusCode
   */
  public int getStatusCode() {
    return statusCode;
  }

  /**
   * @param statusCode
   *          the statusCode to set
   */
  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message
   *          the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }
}
