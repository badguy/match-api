package com.accepted.interview.model;

/**
 * Enum values for all supported sports by the API.
 * 
 * @author Nikos Nodarakis
 *
 */
public enum Sport {

  FOOTBALL(1), BASKETBALL(2);

  private Integer code;

  private Sport(Integer code) {
    this.code = code;
  }

  public Integer getCode() {
    return code;
  }
}
