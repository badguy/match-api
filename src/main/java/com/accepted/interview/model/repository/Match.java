package com.accepted.interview.model.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.accepted.interview.model.Sport;
import com.accepted.interview.model.dto.MatchDto;

/**
 * An entity class that represents a match table on the database.
 * 
 * @author Nikos Nodarakis
 *
 */
@Entity
@Table(name = "match")
public class Match {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true)
  private Long id;

  @Column(name = "description", nullable = false, unique = false)
  private String description;

  @Column(name = "match_date", nullable = false, unique = false)
  private LocalDate matchDate;

  @Column(name = "match_time", nullable = false, unique = false)
  private LocalTime matchTime;

  @Column(name = "team_a", nullable = false, unique = false)
  private String teamA;

  @Column(name = "team_b", nullable = false, unique = false)
  private String teamB;

  @Column(name = "sport", nullable = false, unique = false)
  private Sport sport;

  @OneToMany(mappedBy = "match", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Set<MatchOdd> matchOdds;

  /**
   * Default constructor.
   */
  public Match() {

  }

  /**
   * Constructor to initialize object from a {@link MatchDto} instance.
   * 
   * @param matchDto
   *          the data transfer object to use to initialize object
   */
  public Match(MatchDto matchDto) {
    updateFrom(matchDto);
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the matchDate
   */
  public LocalDate getMatchDate() {
    return matchDate;
  }

  /**
   * @param matchDate
   *          the matchDate to set
   */
  public void setMatchDate(LocalDate matchDate) {
    this.matchDate = matchDate;
  }

  /**
   * @return the matchTime
   */
  public LocalTime getMatchTime() {
    return matchTime;
  }

  /**
   * @param matchTime
   *          the matchTime to set
   */
  public void setMatchTime(LocalTime matchTime) {
    this.matchTime = matchTime;
  }

  /**
   * @return the teamA
   */
  public String getTeamA() {
    return teamA;
  }

  /**
   * @param teamA
   *          the teamA to set
   */
  public void setTeamA(String teamA) {
    this.teamA = teamA;
  }

  /**
   * @return the teamB
   */
  public String getTeamB() {
    return teamB;
  }

  /**
   * @param teamB
   *          the teamB to set
   */
  public void setTeamB(String teamB) {
    this.teamB = teamB;
  }

  /**
   * @return the sport
   */
  public Sport getSport() {
    return sport;
  }

  /**
   * @param sport
   *          the sport to set
   */
  public void setSport(Sport sport) {
    this.sport = sport;
  }

  /**
   * @return the matchOdds
   */
  public Set<MatchOdd> getMatchOdds() {
    return matchOdds;
  }

  /**
   * @param matchOdds
   *          the matchOdds to set
   */
  public void setMatchOdds(Set<MatchOdd> matchOdds) {
    this.matchOdds = matchOdds;
  }

  /**
   * Update the information of the current object by using the provided data
   * transfer object.
   * 
   * @param matchDto
   *          the data transfer object to use to update the fields of the
   *          current object
   */
  public void updateFrom(MatchDto matchDto) {
    this.description = matchDto.getDescription();
    this.matchDate = matchDto.getMatchDate();
    this.matchTime = matchDto.getMatchTime();
    this.teamA = matchDto.getTeamA();
    this.teamB = matchDto.getTeamB();
    this.sport = matchDto.getSport();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    Match other = (Match) obj;
    return other.getId().equals(this.id);
  }
}
