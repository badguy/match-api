package com.accepted.interview.model.repository;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.accepted.interview.model.dto.MatchDto;
import com.accepted.interview.model.dto.OddDto;

/**
 * An entity class that represents a match odds table on the database.
 * 
 * @author Nikos Nodarakis
 *
 */
@Entity
@Table(name = "match_odds")
public class MatchOdd {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "match_id", nullable = false)
  private Match match;

  @Column(name = "specifier", nullable = false, unique = false)
  private String specifier;

  @Column(name = "odd", nullable = false, unique = false)
  private BigDecimal odd;

  /**
   * Default constructor
   */
  public MatchOdd() {

  }

  /**
   * Constructor to initialize object from a {@link MatchDto} and {@link Match}
   * instance.
   * 
   * @param match
   *          the match entity where this odd entity relates to
   * @param matchDto
   *          the data transfer object to use to initialize object
   */
  public MatchOdd(Match match, OddDto oddDto) {
    updateFrom(match, oddDto);
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the match
   */
  public Match getMatch() {
    return match;
  }

  /**
   * @param match
   *          the match to set
   */
  public void setMatch(Match match) {
    this.match = match;
  }

  /**
   * @return the specifier
   */
  public String getSpecifier() {
    return specifier;
  }

  /**
   * @param specifier
   *          the specifier to set
   */
  public void setSpecifier(String specifier) {
    this.specifier = specifier;
  }

  /**
   * @return the odd
   */
  public BigDecimal getOdd() {
    return odd;
  }

  /**
   * @param odd
   *          the odd to set
   */
  public void setOdd(BigDecimal odd) {
    this.odd = odd;
  }

  /**
   * Update the information of the current object by using the provided objects
   * 
   * @param match
   *          the match entity where this odd entity relates to
   * @param matchDto
   *          the data transfer object to use to initialize object
   */
  public void updateFrom(Match match, OddDto oddDto) {
    this.match = match;
    this.specifier = oddDto.getSpecifier();
    this.odd = oddDto.getOdd();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || this.getClass() != obj.getClass()) {
      return false;
    }
    MatchOdd other = (MatchOdd) obj;
    return other.getId().equals(this.id);
  }
}
