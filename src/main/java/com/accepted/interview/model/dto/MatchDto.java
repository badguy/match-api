package com.accepted.interview.model.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.validation.constraints.NotNull;

import org.springframework.hateoas.RepresentationModel;

import com.accepted.interview.model.Sport;
import com.accepted.interview.model.repository.Match;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The data transfer object for match information. It extends the
 * {@link RepresentationModel} class in order to be eligible to used for
 * pagination purposes. Also contains validation and json formatting rules on
 * its fields.
 * 
 * @author Nikos Nodarakis
 *
 */
public class MatchDto extends RepresentationModel<MatchDto> {

  @NotNull
  private String description;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
  private LocalDate matchDate;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
  private LocalTime matchTime;

  @NotNull
  private String teamA;

  @NotNull
  private String teamB;

  @NotNull
  private Sport sport;

  /**
   * Default constructor.
   */
  public MatchDto() {

  }

  /**
   * Constructor to initialize object from a {@link Match} instance.
   * 
   * @param match
   *          the match entity to use to initialize object
   */
  public MatchDto(Match match) {
    this.description = match.getDescription();
    this.matchDate = match.getMatchDate();
    this.matchTime = match.getMatchTime();
    this.teamA = match.getTeamA();
    this.teamB = match.getTeamB();
    this.sport = match.getSport();
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the matchDate
   */
  public LocalDate getMatchDate() {
    return matchDate;
  }

  /**
   * @param matchDate
   *          the matchDate to set
   */
  public void setMatchDate(LocalDate matchDate) {
    this.matchDate = matchDate;
  }

  /**
   * @return the matchTime
   */
  public LocalTime getMatchTime() {
    return matchTime;
  }

  /**
   * @param matchTime
   *          the matchTime to set
   */
  public void setMatchTime(LocalTime matchTime) {
    this.matchTime = matchTime;
  }

  /**
   * @return the teamA
   */
  public String getTeamA() {
    return teamA;
  }

  /**
   * @param teamA
   *          the teamA to set
   */
  public void setTeamA(String teamA) {
    this.teamA = teamA;
  }

  /**
   * @return the teamB
   */
  public String getTeamB() {
    return teamB;
  }

  /**
   * @param teamB
   *          the teamB to set
   */
  public void setTeamB(String teamB) {
    this.teamB = teamB;
  }

  /**
   * @return the sport
   */
  public Sport getSport() {
    return sport;
  }

  /**
   * @param sport
   *          the sport to set
   */
  public void setSport(Sport sport) {
    this.sport = sport;
  }

  @Override
  public String toString() {
    return "MatchDto [description=" + description + ", matchDate=" + matchDate + ", matchTime="
        + matchTime + ", teamA=" + teamA + ", teamB=" + teamB + ", sport=" + sport + "]";
  }
}
