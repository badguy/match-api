package com.accepted.interview.model.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.accepted.interview.model.repository.MatchOdd;
import com.accepted.interview.serializer.BigDecimalDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * The data transfer object for match odd information. Also contains validation
 * and json deserialization rules on its fields.
 * 
 * @author Nikos Nodarakis
 *
 */
public class OddDto {

  @NotNull
  private String specifier;

  @NotNull
  @JsonDeserialize(using = BigDecimalDeserializer.class)
  private BigDecimal odd;

  /**
   * Default constructor.
   */
  public OddDto() {

  }

  /**
   * Constructor to initialize object from a {@link MatchOdd} instance.
   * 
   * @param matchOdd
   *          the match odd entity to use to initialize object
   */
  public OddDto(MatchOdd matchOdd) {
    this.specifier = matchOdd.getSpecifier();
    this.odd = matchOdd.getOdd();
  }

  /**
   * @return the specifier
   */
  public String getSpecifier() {
    return specifier;
  }

  /**
   * @param specifier
   *          the specifier to set
   */
  public void setSpecifier(String specifier) {
    this.specifier = specifier;
  }

  /**
   * @return the odd
   */
  public BigDecimal getOdd() {
    return odd;
  }

  /**
   * @param odd
   *          the odd to set
   */
  public void setOdd(BigDecimal odd) {
    this.odd = odd;
  }

  @Override
  public String toString() {
    return "OddDto [specifier=" + specifier + ", odd=" + odd + "]";
  }
}
